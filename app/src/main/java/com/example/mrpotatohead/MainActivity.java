package com.example.mrpotatohead;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.CompoundButton;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    private ImageView body, beard, moustache, imageView, eyebrow, hair;
    private CheckBox cbhair, cbmoustache, cbeyebrow, cbbeard;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        body = findViewById(R.id.body);
        beard = findViewById(R.id.beard);
        moustache = findViewById(R.id.moustache);
        imageView = findViewById(R.id.imageView);
        eyebrow = findViewById(R.id.eyebrow);
        hair = findViewById(R.id.hair);

        cbhair = findViewById(R.id.cbhair);
        cbmoustache = findViewById(R.id.cbmoustache);
        cbeyebrow = findViewById(R.id.cbeyebrow);
        cbbeard = findViewById(R.id.cbbeard);

        textView = findViewById(R.id.textView);


        cbhair.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    hair.setVisibility(View.VISIBLE);
                } else {
                    hair.setVisibility(View.INVISIBLE);
                }
            }
        });

        cbmoustache.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    moustache.setVisibility(View.VISIBLE);
                } else {
                    moustache.setVisibility(View.INVISIBLE);
                }
            }
        });

        cbeyebrow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    eyebrow.setVisibility(View.VISIBLE);
                } else {
                    eyebrow.setVisibility(View.INVISIBLE);
                }
            }
        });

        cbbeard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    beard.setVisibility(View.VISIBLE);
                } else {
                    beard.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}

