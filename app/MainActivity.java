import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ImageView Body, Body, moustache, imageView, eyebrow, hair;
    private CheckBox cbhair, cbmoustache, cbeyebrow, cbbeard;
    private TextView nama, nim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        body = findViewById(R.id.body);
        beard = findViewById(R.id.beard);
        moustache = findViewById(R.id.moustache);
        imageView = findViewById(R.id.imageView);
        eyebrow = findViewById(R.id.eyebrow);
        hair = findViewById(R.id.hair);


        cbhair = findViewById(R.id.cbhair);
        cbmoustache = findViewById(R.id.cbmoustache);
        cbeyebrow = findViewById(R.id.cbeyebrow);
        cbbeard = findViewById(R.id.cbbeard);


        nama = findViewById(R.id.nama);
        nim = findViewById(R.id.nim);
    }
}
